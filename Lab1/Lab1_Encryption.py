import argparse
import random

# Iteration as specified in assignment
# Caution: if key is bx00000000, encryption does nothing (A^0 = A)
def iterateCipher(left, right, key):
	return right, left^key

# for each key in the list, iterate the cipher with that key.
# In main, key is read as 8 values.
def encryptLRPair(left, right, keyList):
	for key in keyList:
		left, right = iterateCipher(left, right, key)
	return left, right
	
def encryptContent(rawContent, keyList):
	# Pad with to make even length, finalize with the number of bytes padded.
	# This tells the decrypter how many bytes to trim from the tail.
	if len(rawContent) % 2 != 0:	
		rawContent.append(1)
	else:
		rawContent.extend(0,2)

	# encrypt each pair of bytes from the input
	encryptedContent = []
	for i in range(0, len(rawContent), 2):
			encryptedContent.extend( encryptLRPair(rawContent[i], rawContent[i+1], keyList) )	
	return bytearray(encryptedContent)

def main():
	#get input and output file names
	parser = argparse.ArgumentParser("Encrypts a file using simplified DES")
	parser.add_argument("inFile")
	parser.add_argument("outFile")
	parser.add_argument("keyFile")
	args = parser.parse_args()
	
	#read the key file, stored as integers
	keyList = []
	try:
		with open(args.keyFile, 'rb') as inFile:
			for i in range(8):
				keyList.append(ord(inFile.read(1)))
	except IOError as e:
		print("Error reading key file: {}".format(e))
	except (IndexError, TypeError) as e:
		print("Error: key file must contain at least 8 bytes")

	#read the input file into a byte array
	try:
		with open(args.inFile, 'rb') as inFile:
			fileContent = bytearray(inFile.read())
	except IOError as e:
		print("Error opening file: {}".format(args.inFile))
		
	#encrypt the array
	try:
		with open(args.outFile, 'wb') as outFile:
			outFile.write(encryptContent(fileContent, keyList))
	except IOError as e:
		print("Error opening file: {}".format(args.outFile))
		

if __name__ == '__main__':
	main()
