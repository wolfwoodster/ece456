import argparse
import random

def iterateDecipher(left, right, key):
	return right^key, left

# for each key in the list, iterate the cipher.
# In main, key is read as 8 values.
def decryptLRPair(left, right, keyList):
	for key in keyList:
		left, right = iterateDecipher(left, right, key)
	return left, right
	
def decryptContent(rawContent, keyList):
	# decrypt each pair of bytes from the input
	decryptedContent = []
	for i in range(0, len(rawContent), 2):
			decryptedContent.extend( decryptLRPair(rawContent[i], rawContent[i+1], keyList) )
			
	# array[:-n] removes n elements from the tail of the array, but does not work for n <= 0
	# "x or y" returns x if x is True otherwise returns y
	# In a slice, None is ignored, so lst[:None] == lst[:]
	# http://stackoverflow.com/a/37105499/813385
	decryptedContent = decryptedContent[:-1-decryptedContent[-1] or None]	
	return bytearray(decryptedContent)

def main():
	#get input and output file names
	parser = argparse.ArgumentParser("Encrypts a file using simplified DES")
	parser.add_argument("inFile")
	parser.add_argument("outFile")
	parser.add_argument("keyFile")
	args = parser.parse_args()
	
	# read the key file, stored as 8 integers
	# 
	keyList = []
	try:
		with open(args.keyFile, 'rb') as inFile:
			for i in range(8):
				keyList.append(ord(inFile.read(1)))
	except IOError as e:
		print("Error reading key file: {}".format(e))
	except (IndexError, TypeError) as e:
		print("Error: key file must contain at least 8 bytes")

	# keys must be applied in the reverse order from encryption
	keyList.reverse()
	
	# read the input file into a byte array so that bytes can be accessed by index
	try:
		with open(args.inFile, 'rb') as inFile:
			fileContent = bytearray(inFile.read())
	except IOError as e:
		print("Error opening file: {}".format(args.inFile))
		
	# decrypt the file content and store to the output file
	try:
		with open(args.outFile, 'wb') as outFile:
			outFile.write(decryptContent(fileContent, keyList))
	except IOError as e:
		print("Error opening file: {}".format(args.outFile))

if __name__ == '__main__':
	main()
