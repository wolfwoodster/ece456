from ipaddress import IPv4Address as ipv4


def parseIP(ipString):
	'''
	ipString is a string representation of an ip address, e.g '192.168.0.1'
	returns a bytearray representation of the values in the address
	'''
	try:
		return bytearray( ipv4(ipString).packed )
	except Exception as e:
		print("Failed to parse IP: {}".format(ipString))
		print(e)
		exit(-1)
		
		
def readFileBytes(filename):
	'''
		reads a file as a binary value and returns a bytearray representation of its contents
		will kill your program if you do dumb things
	'''
	try:
		with open(filename, 'rb') as inFile:
			return bytearray(inFile.read())
	except IOError as e:
		print("Error opening file: {}".format(filename))
		print(e)
		exit(-1)
		
		
def onesComplementSum(A, wordBytes):
	'''
	A is sequence of bytes
	wordBytes is the number of bytes per word in the sequence
	len(A) must be divisible by wordBytes
	'''
	limit = 1<<(wordBytes*8)	# any result greater than or equal to this has overflow bits to add in
	values = [int.from_bytes(A[i:i+wordBytes], byteorder='big') for i in range(0,len(A),2)]	# interpret words as list of ints
	currentSum = sum(values) 	#calculate the sum	
	while True: 				# add the overflow back in until no overflow bytes remain
		currentSum = currentSum + currentSum>>(wordBytes*8)
		if(currentSum < limit):
			break
	# return the 1's complement of the result (
	return currentSum^0xFFFF