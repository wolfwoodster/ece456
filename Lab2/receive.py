import argparse
from utils import *


def processDatagram(datagram, sourceIP, destIP):
	print("Received\n{}".format(datagram[0:8]))
	sourcePort 	= datagram[0:2]
	destPort 	= datagram[2:4]
	totalLength = datagram[4:6]
	checkSum	= datagram[6:8]
	paddedData	= datagram[8:]
	pseudoHeader= parseIP(sourceIP) + parseIP(destIP) + bytearray([0]) +  bytes([17]) + totalLength
	datagram[6], datagram[7] = 0, 0
	
	
def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Creates a UDP datagram from an input file")
	parser.add_argument("datagramFile", help="File containing data to be sent")
	parser.add_argument("sourceIP", help="Source IP address")
	parser.add_argument("destIP", help="Destination IP address")
	parser.add_argument("outputFile", help="the file to write the decrypted payload to")
	return parser.parse_args()

def writeData(data):
	pass

def main():
	args = getArgs()
	datagram = readFileBytes(args.datagramFile)
	data = processDatagram(datagram, args.sourceIP, args.destIP)
	writeData(data)

if __name__ == '__main__':
	main()