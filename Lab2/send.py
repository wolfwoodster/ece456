import argparse
from struct import pack
from utils import *

def parsePort(portString):
	portNum = int(portString)
	return bytearray([(portNum >> 8) & 0xff,portNum & 0xff])

def bytesToInts(A, wordLength):
	'''
	Parses a bytes object into a list of wordLength words interpreted as integers
	params:
		A: 			a bytes object
		wordLength: the length of words to be parsed as integers
	return:
		array of integer representations for each word in A
	'''
	return [int.from_bytes(Ai, byteorder='big')]
	

def fillCheckSum(pseudoheader, partial):
	'''
	takes in a datagram where all fields are complete except the checksum of b'00'
	'''
	checkGram = pseudoheader + partial				#checksum is calculated with a pseudoheader concatenated to the datagram with 0s for the checksum
	checkSumValue = onesComplementSum(checkGram, 2)	# calculate the value of the check sum
	print('partial: {}'.format( partial))
	print('checkSum: {}'.format( checkSumValue))
	checkSumBytes = pack('>H', checkSumValue)
	print(partial)
	partial[6] = checkSumBytes[0]
	partial[7] = checkSumBytes[1]
	return partial
	

def makeDatagram(content):
	# Calculate the total length and store in a byte array, then pad the data with 0's.
	totalLength = bytearray(pack('>H', len(content['fileBytes']) + 8))			
	data = content['fileBytes'] + (len(content['fileBytes'])%2)*b'0'
	
	# create pseudoheader and datagram with zero checksum. Return the datagram with the checksum populated.
	pseudoheader = content['sourceIP'] + content['destIP'] + bytearray([0]) +  bytes([17]) + totalLength
	tempDatagram = content['sourcePort'] + content['destPort'] + totalLength + bytearray([0,0]) + data
	return fillCheckSum(pseudoheader, tempDatagram)
	

def writeDatagram(datagram, datagramFile):
	try:
		with open(datagramFile, 'wb') as outFile:
			outFile.write(datagram)
	except IOError as e:
		print("Error opening file: {}".format(args.dataFile))
		exit(-1)


def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Creates a UDP datagram from an input file")
	parser.add_argument("dataFile", help="File containing data to be sent")
	parser.add_argument("sourceIP", help="Source IP address")
	parser.add_argument("destIP", help="Destination IP address")
	parser.add_argument("sourcePort", help="Source Port")
	parser.add_argument("destPort", help="Destination Port")
	parser.add_argument("datagramFile", help="output file containing datagram for the given data file. Must end in .bin")
	return parser.parse_args()


def main():
	# get command line inputs
	args = getArgs()
	
	#parse inputs into bytes objects
	datagramContent = {
		'sourceIP' 	: parseIP(args.sourceIP),
		'destIP' 	: parseIP(args.destIP),
		'sourcePort': parsePort(args.sourcePort),
		'destPort' 	: parsePort(args.destPort),
		'fileBytes' : readFileBytes(args.dataFile)
	}	
	
	# create datagram and write it to the designated output file
	datagram = makeDatagram(datagramContent) 	
	writeDatagram(datagram, args.datagramFile)
	
	exit(0)


if __name__ == '__main__':
	main()
	
