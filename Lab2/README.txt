Application:
Python version 3.x

Key file:
any file containing at least 8 bytes may be used as a key.
The encryption is symmetric so encryption and decryption
must use the same key file.

Encryption Syntax:
python Lab1_Encryption.py <input file> <output file> <key file>
e.g. Python3 Lab1_Encryption.py gatsby.txt encrypted.txt key.txt

Decryption Syntax:
python Lab1_Decryption.py <input file> <output file> <key file>
e.g. Python3 Lab1_Encryption.py encrypted.txt decrypted.txt key.txt