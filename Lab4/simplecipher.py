from utils import *

class Cipher():
  def __init__(self, key=b'abc123y;'):
    self.encrKey = None
    self.decrKey = None
    self.setKeys(key)


  def decrypt(self, cipherMessage):
    return decryptContent(cipherMessage, self.decrKey)

  def encrypt(self, plainMessage):
    return encryptContent(plainMessage, self.encrKey)

  def setKeys(self, key):
    '''
    Returns True if successfully assigned, False otherwise.
    '''
    if isinstance(key, bytes):
      key = bytearray(key)
    elif isinstance(key, str):
      key = bytes(key, 'utf-8')
    if isinstance(key, bytearray) and len(key) >= 8:
      self.encrKey = key[:8]
      self.decrKey = self.encrKey[:]
      self.decrKey.reverse()
      return True
    else:
      return False


def decryptLRPair(left, right, keyList):
  for key in keyList:
    left, right = right^key, left
  return left, right
  

def decryptContent(rawContent, keyList):
  decryptedContent = []
  for i in range(0, len(rawContent), 2):
      decryptedContent.extend( decryptLRPair(rawContent[i], rawContent[i+1], keyList) )
  decryptedContent = decryptedContent[:-decryptedContent[-1] or None]
  return bytearray(decryptedContent)


# for each key in the list, iterate the cipher with that key.
# In main, key is read as 8 values.
def encryptLRPair(left, right, keyList):
  for key in keyList:
    left, right = right, left^key
  return left, right
  
def encryptContent(rawContent, keyList):
  # Ensure content is a bytearray
  rawContent = makeByteArray(rawContent)
  # Pad width to make even length, finalize with the number of bytes padded.
  # This tells the decrypter how many bytes to trim from the tail.
  if len(rawContent) % 2 != 0:  
    rawContent.append(1)
  else:
    rawContent.extend([0,2])
  # encrypt each pair of bytes from the input
  encryptedContent = []
  for i in range(0, len(rawContent), 2):
      temp = encryptLRPair(rawContent[i], rawContent[i+1], keyList)
      encryptedContent.extend( temp )
  return bytearray(encryptedContent)