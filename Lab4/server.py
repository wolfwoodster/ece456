import argparse
from utils import *
import udpsock
import datetime

class Server():
	def __init__(self, sock):
		self.messageHistory = []
		self.sock = sock

	def addToHistory(self, msg, addr):
		'''
		Adds new message data to the  history and removes excess entries.
		'''
		entry = {
			'timestamp': datetime.datetime.now().time().strftime("%H:%M:%S"),
			'message': msg,
			'originator': addr[0] + ':' + str(addr[1])
		}
		self.messageHistory.append(entry)	# add message to history
		while len(self.messageHistory) > 5:	# keep only 5 most recent messages
			del self.messageHistory[0]
	
	def getHistoryString(self):
		'''
		Converts the history to a human readable string, if that human is actually a robot.
		'''
		historyString = ''
		for entry in self.messageHistory:
			historyString += '{:>20} @ {:<8} --> {}\n'.format(
				str(entry['originator']),
				entry['timestamp'],
				entry['message']				
			)
		return historyString
	
	def sendHistory(self, destination):
		historyString = self.getHistoryString()
		self.sock.send(historyString, destination)	
		
	def checkQuit(self, msg):
		'''
		Checks if a message was a command to close the server.
		'''
		if(msg == '#serverclose'):
			print("Client requested server close. Terminating.")
			exit(0)	

	def listen(self, timeout):
		'''
		Listens for a message from a client.
		Updates history with the message, originator, and timestamp.
		Responds with the current message history.
		'''
		print("Listening on ", self.sock.getAddress())
		while True:				
			msg = self.sock.receive(timeout)
			if msg is not None:
				msg, remoteAddr = msg
				msg = msg.decode('utf-8')
				print("{:<25}: {:}".format(str(remoteAddr), msg))
				self.checkQuit(msg)				# if message was quit, terminate without response
				self.addToHistory(msg, remoteAddr)
				self.sendHistory(remoteAddr)
				
				
def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Simple echo server")
	parser.add_argument("--serverHost", default='', help="The server's i.p. address")
	parser.add_argument("--serverPort", default=17000, type=int, help="The server's port")
	parser.add_argument("--keyfile", default = "key.txt", help="File containing at least 8 bytes for an encryption key")
	parser.add_argument("--timeout", default=1, help="""Time before stopping a listen cycle.
						None: blocking receive.
						0: not allowed, will reset to 1.
						>0: blocking receive in 1 second intervals.
						It is recommended to use intervals to allow keyboard interrupts.""")
	return parser.parse_args()



def main():
	args = getArgs()
	key  = getKey(args.keyfile)	
	sock = udpsock.Udpsock(args.serverHost, args.serverPort, key )
	server = Server(sock)
	timeout = args.timeout if args.timeout == 0 else 1 # do not allow nonblocking receive
	server.listen(timeout)
	sock.close()
				
	
if __name__ == '__main__':
	main()