Application:
Python version 3.x

Modified Echo Server

Clients send messages to the server.
Server responds with last 5 (at most) messages received.


###########################################################
===========================================================
						server.py
-----------------------------------------------------------
Listens for messages from clients. Responds with the last
5 (max) messages received.
-----------------------------------------------------------
All parameters are optional.
Defaults:
	serverHost: ''
	serverPort: 17000, type=int
	keyfile: 	"key.txt"
	timeout: 	None
	
Using default parameters will create a server listening
on all interfaces on port 17000 with a 1 second timeout.
Messages are encrypted and decrypted with key.txt.

Use python server.py -h for more information on parameters.


###########################################################
===========================================================
						client.py
-----------------------------------------------------------
Sends a message to the designated server and waits for
a response. Displays the response, or a message indicating
failure.
-----------------------------------------------------------
All parameters are optional.
Defaults:
	destHost	:127.0.0.1
	destPort	:17000
	sourceHost	:''
	sourcePort	:16000
	keyfile		:key.txt
	datafile	:None

By default the application will create a socket listening
on port 16000 on all interfaces, and assign 127.0.0.1:17000
as the server address. The client then goes into keyboard
entry mode.

Keyboard entry mode (default):
	loop to request and send messages entered at keyboard
	#clientclose shuts down the client
	#serverclose shuts down a blocking server
	
data file mode:
    sends the file to the server then closes.

Use python server.py -h for more information on parameters.


###########################################################
===========================================================
						simplecipher.py
-----------------------------------------------------------
Module containing a Cipher class. Instantiating the class
with a key allows use of cipher encrypt and decrypt
functions.
-----------------------------------------------------------
Not runnable


###########################################################
===========================================================
						udpsock.py
-----------------------------------------------------------
Creates a custom socket object for use by server and client
-----------------------------------------------------------
Not runnable


###########################################################
===========================================================
						utils.py
-----------------------------------------------------------
generic utility functions
-----------------------------------------------------------
Not runnable
