import socket
import simplecipher
from utils import *


class Udpsock():
	def __init__(self, host, port,  key='asdfjkl;'):
		self.BUFFERLIMIT = 65536	# send/receive buffer size
		self.cipher = simplecipher.Cipher(key)	# cipher for encryption
		self.makeSocket((host, port))
		
	def getKeys(self):
		return (self.cipher.encrKey, self.cipher.decrKey)
	
	def setKeys(self, key):
		return self.cipher.setKey(key)

	def makeSocket(self, address):
		''' (re) establish socket with <address> bound '''
		try:	# close the socket if it's open
			self.sock.close()
		except:
			pass
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.bind(address)


	def close(self):
		self.sock.close()

	
	def getAddress(self):
		return self.sock.getsockname()


	def receive(self, timeout=None):
		'''
		Receives on the active socket.
			timeput:
				None: blocking
				0: nonblocking
				positive nonzero: timeout in seconds (float)
		'''
		self.sock.settimeout(timeout)	
		try:			
			msg, remoteAddr = self.sock.recvfrom(self.BUFFERLIMIT)
			decmsg = self.cipher.decrypt(msg)
			return (decmsg, remoteAddr)
		except socket.timeout as e:
			return None
	
	def checkAck(self, ack, remoteAddr):
		if not ack:
			return False
		else:
			msg, addr = ack
			if msg == b'06' and addr == remoteAddr:
				return True
			else:
				return False
	
	def sendAck(self, address):
		ack = self.cipher.encrypt(b'06')
		self.sock.sendto(ack, address)
	
	def send(self, message, dest, timeout=1, retries=0):
		'''
		Sends a UDP datagram to <dest> address.
		PARAMETERS
			retries: maximum number of attempts to resend datagram if no response.
			timeout: time the socket will wait before assuming a datagram was lost.
			message: string, bytes, or bytearray containing the message to send.
			encrypt: if True, message is encrypted with simplecipher before sending.
		RETURNS
			False: cannot confirm receipt (always false if retries is 0)
			True: receipt confirmed			
		'''	
		message = makeByteArray(message)			# ensure message is bytearray object
		message = self.cipher.encrypt(message)		# encrypt message		
		self.sock.sendto(message, dest)				# send message
		
		# if retries > 0, verify and repeat if no ack
		for i in range(retries):
			try:
				msg = self.receive(timeout)
				if msg == None:
					print("Timeout: retrying ({})".format(i))
					self.sock.sendto(message, dest)
				else:
					return msg
			except OSError as e:
					print("Socket Error: retrying ({})".format(i))	# connection reset by remote host, ignore
			except Exception as e:
				print('Unhandled exception:', e)
				exit(-1)
		else:
			return None

	