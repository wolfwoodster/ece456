import argparse
from utils import *
import udpsock

class Client():
	def __init__(self, sock):
		self.sock = sock
	
	def sendFile(self, destAddress, filename):
		fileContents  = readFileBytes(filename)
		if fileContents != None:
			print('sending {} to {}:{}'.format(filename, destAddress[0], destAddress[1]))
			self.sendMessage(fileContents, destAddress)
		
	def sendFromKeyboard(self, destAddress):
		msg = input("Enter the message to send: ")
		if msg == '#clientclose':
			return False
		else:
			if msg == '#serverclose':
				self.sendMessage(msg, destAddress, retries=0)
			else:
				self.sendMessage(msg, destAddress)
			return True
		

	def keyboardLoop(self, destAddress):
		while True:
			if not self.sendFromKeyboard(destAddress):
				break;
	
	def sendMessage(self, msg, destAddress, timeout=1, retries=5):
		try:
			reply = self.sock.send(msg, destAddress,timeout, retries)
			response = reply[0].decode('utf-8')
		except:
			response = None
		print('Response from server:')
		print(response)


def getArgs():
	#get input and output file names
	parser = argparse.ArgumentParser("Simple client for echo server test")
	parser.add_argument("--destHost", default="127.0.0.1", help="ip address")
	parser.add_argument("--destPort", default=17000, type=int, help='server port')
	parser.add_argument("--sourceHost", default='', help='interface to listen for responses')
	parser.add_argument("--sourcePort", default=16000, type=int, help='port to listen for responses')
	parser.add_argument("--keyfile", default = "key.txt", help='file containing at least 8 bytes for encryption key')
	parser.add_argument("--datafile", default = None, help="""File to be sent as a one-off message. If None, client enters keyboard mode.
						In keyboard mode, use #clientclose to stop the client.
						Use #serverclose to stop a blocking server""")
	return parser.parse_args()


def main():
	args = getArgs()
	serverAddress = (args.destHost, args.destPort)
	key  = getKey(args.keyfile)	
	sock = udpsock.Udpsock(args.sourceHost, args.sourcePort, key)
	print("Socket opened at: ", sock.getAddress())
	client = Client(sock)	
	if args.datafile == None:
		client.keyboardLoop(serverAddress)
	else:
		client.sendFile(serverAddress, args.datafile)	
	sock.close()

				
	
if __name__ == '__main__':
	main()