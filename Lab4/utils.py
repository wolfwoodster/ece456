def getKey(keyfile):
	key = readFileBytes(keyfile)
	if key == None:
		key = input("Missing or invalid key file. Enter key:").encode('utf-8')
	while len(key) < 8:
		key = input("key must be at least 8 charcters. Enter key:").encode('utf-8')
	return key[:8]

def makeByteArray(message):
	'''
	Returns a bytearray interpretation of the input message. 
	'''
	if isinstance (message, str):
		return bytearray(message, 'utf-8')
	elif isinstance (message, bytes):
		return bytearray(message)
	elif isinstance (message, bytearray):
		return message
	else:
		print(message)
		raise TypeError(type(message),"is not bytes, bytearray, or string")
		
		
def readFileBytes(filename):
	'''
		reads a file as a binary value and returns a bytearray representation of its contents
		will kill your program if you do dumb things
	'''
	try:
		with open(filename, 'rb') as inFile:
			return bytearray(inFile.read())
	except IOError as e:
		return None
